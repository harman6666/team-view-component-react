# Team View Component with ReactJS || [![PRs Welcome](https://img.shields.io/badge/PRs-welcome-brightgreen.svg)](https://gitlab.com/harman6666/team-view-component-react)
------------

Introduction
------------

This is not just a team-viewer-component it is moreover a small webapp for adding or removing team members from the list. It was written with ReactJS. Please have a look below to get more details regarding this webapp.
 
Components
------------
- Having a welcome page which greets the TestBird and asks a question that in which view you want to go first?
- Having the __team view__ component which is divided into many other small components due to modular approach.
- Having __main__ component which have all the pagination links associated with it.
- Having __PageNotFound__ component which will display only when React not able to find a particular component attached to the router.

Architecture Details
------------
- Imagine for a moment an app with only one file for all your components, the store, utilities, everything.
A terrible idea, of course. The problem with the large number of files is to navigate them.
So, For this reason I structured the application in an effiecent manner.
##### Directory Structure :-
![directory-structure](https://s9.postimg.cc/sshop8bm7/app_structure.png)

- As you can see in the above screenshot we have different folders for different components and every component have index.js file because this is the default file and when we run the application this file will automatically call when we import this folder in any other component.
- Made separate folder for test files with "__" in the starting and ending because jest will automatically pick all the files inside this folder(there is a configuration in the webpack) and run tests written in these files. 

Functionalities
------------

- User can be able to see homepage with two buttons "Native or Advance" for selecting landing view that on which view user wants to land first.
- Native view, here user can be able to see multiple options in the native form __<select>__ tag and have functionality to select any option from the drop-down which will then add to the below list.
- Advance view, here all the other functionalities are same except the dropdown. In this advance dropdown we have different functionalities. 
- In advance view, user can be able to search any name in the search field. If there is any name realted to the searched value than user can be able to see the results in the dropdown below to __search field__.
- In advance view, if no results are there for the searched value than user can be able to see the __No result msg__.
- User can remove any member from the visible list by clicking on the cross icon which will be visible on the hover of the grid. By removing, the data again added to the array list for further use.
- User can be able to see __"show all"__ button if visible results are more than 5.

Installation
-------------

- Clone/Download the repository by using [GitLab Link](https://gitlab.com/harman6666/team-view-component-react.git)
- After the 1st step _run __npm install___ in the project folder.
- After all the necessary packages got installed then _run __npm start___ the command prompt.
- We can also build the project for the production release by using __npm build__ command.
- Yeah, that's it. Enjoy the webapp.

Some Examples/Screenshots
-----------


![homepage-view](https://s9.postimg.cc/mz31aiagf/Test_Birds_homepage.png)
![intital-view](https://s9.postimg.cc/5yk51u54v/Initial_view.png)
![native-view](https://s9.postimg.cc/khra38b4f/Native_view.png)
![advance-view](https://s9.postimg.cc/oe4lz7oe7/Advance_view.png)
![showAll-view](https://s9.postimg.cc/b9z1mjbrz/View_with_showall.png)


Some NPM commands for reference
---------------

- __npm start__ starts the development server and auto-reloads the page any time you make edits
- __npm run__ build bundles the app into static files for production
- __npm test__ starts the test runner and lets you test your app with Jest as you build it.
- __npm eject__ we can ejet the project and make more custom configuration as per the project requirement.


### Many Thanks for looking !




